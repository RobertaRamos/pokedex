package com.example.pokedex.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.pokedex.dao.PokemonDao
import com.example.pokedex.model.EntidadePokemon
import com.example.pokedex.model.Pokemon

@Database(entities = [EntidadePokemon::class], version = 1, exportSchema = false)
abstract class PokemonDataBase: RoomDatabase(){

    abstract fun getPokemonDao(): PokemonDao

    companion object{
        fun getInstance(context: Context):PokemonDataBase{
            return Room.databaseBuilder(context, PokemonDataBase::class.java, "pokemon.db").build()
        }
    }
}
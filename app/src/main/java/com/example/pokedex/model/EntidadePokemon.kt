package com.example.pokedex.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
class EntidadePokemon(val next: String? = null,
                      val name: String? = null,
                      val url: String? = null,
                      val abilities: String? = null,
                      val height: Double? = 0.0,
                      val weight: Double = 0.0,
                      val tipo: String? = null) {

    @PrimaryKey(autoGenerate = true)
    var id = 0
}
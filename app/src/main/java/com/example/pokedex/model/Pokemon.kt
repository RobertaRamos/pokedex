package com.example.pokedex.model

import java.io.Serializable


class Pokemon(val name: String,
              val url: String) : Serializable {

}

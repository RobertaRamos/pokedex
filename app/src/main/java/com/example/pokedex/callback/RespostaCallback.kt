package com.example.pokedex.callback

import com.example.pokedex.retrofit.RetornoPokemonApiDetalhes

interface RespostaCallback {

    fun sucesso(detalhes: RetornoPokemonApiDetalhes)

    fun falha (menssagemErro: String)
}
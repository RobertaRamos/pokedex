package com.example.pokedex.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.pokedex.model.EntidadePokemon

@Dao
interface PokemonDao {

    @Insert(onConflict = REPLACE)
    fun salva(pokemon: EntidadePokemon)

    @Delete
    fun remove(entidadePokemon: EntidadePokemon)

    @Query("SELECT * FROM entidadepokemon WHERE name = :nome")
    fun buscaPokemon(nome: String): LiveData<EntidadePokemon?>

    @Insert(onConflict = REPLACE)
    fun salvaInterno(results: List<EntidadePokemon>)

    @Query("SELECT * FROM entidadepokemon")
    fun buscaTodos(): List<EntidadePokemon>

}


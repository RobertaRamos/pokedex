package com.example.pokedex.retrofit.service

import com.example.pokedex.retrofit.RetornoPokemonAPI
import com.example.pokedex.retrofit.RetornoPokemonApiDetalhes
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonService {

    @GET("pokemon/{id}")
    fun buscaDetalhesPokemon(@Path("id") id: Int): Call<RetornoPokemonApiDetalhes>

    @GET("pokemon")
    suspend fun buscaPaginadaPokemon(
        @Query("limit")limit: Int = 10,
        @Query("offset")offset: Int): RetornoPokemonAPI
}
package com.example.pokedex.retrofit

import com.example.pokedex.retrofit.service.PokemonService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PokemonRetrofit {
    private val baseURL = "https://pokeapi.co/api/v2/"

    private val retrofit = Retrofit.Builder()
        .baseUrl(baseURL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun getPokemonServices(): PokemonService{
        return retrofit.create(PokemonService::class.java)
    }
}
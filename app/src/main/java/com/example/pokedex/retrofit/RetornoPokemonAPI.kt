package com.example.pokedex.retrofit

import com.example.pokedex.model.Pokemon

class RetornoPokemonAPI(val next: String?,
                        val results: List<Pokemon>)
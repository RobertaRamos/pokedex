package com.example.pokedex.retrofit

class RetornoPokemonApiDetalhes(val abilities: List<AbilidadesPokemon>,
                                val height: Double,
                                val weight: Double,
                                val types: List<TipoPokemon>)
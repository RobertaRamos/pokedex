package com.example.pokedex.network

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.pokedex.dao.PokemonDao
import com.example.pokedex.model.EntidadePokemon
import com.example.pokedex.model.Pokemon
import com.example.pokedex.retrofit.service.PokemonService
import java.lang.Exception

class PokemonPagingSource(private val service: PokemonService,
                          private val dao: PokemonDao) : PagingSource<Int, Pokemon>() {

    override fun getRefreshKey(state: PagingState<Int, Pokemon>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Pokemon> {
        return try {
            vericaProximaPaginaOffset(params)

        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    private suspend fun vericaProximaPaginaOffset(params: LoadParams<Int>): LoadResult.Page<Int, Pokemon> {
        val nextPage: Int = params.key ?: OFFSET_PAGE_INDEX
        val response = service.buscaPaginadaPokemon(offset = nextPage)
        var proximoOffesetNumber: Int? = null
        if (response.next != null) {
            val uri = Uri.parse(response.next)
            val proximoOffset = uri.getQueryParameter("offset")
            proximoOffesetNumber = proximoOffset?.toInt()
        }

        return LoadResult.Page(
            data = response.results,
            prevKey = null,
            nextKey = proximoOffesetNumber
        )


    }

    companion object {
        private const val OFFSET_PAGE_INDEX = 0
    }
}


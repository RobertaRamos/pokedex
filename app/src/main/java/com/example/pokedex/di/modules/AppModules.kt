package com.example.pokedex.di.modules

import androidx.room.Room
import com.example.pokedex.dao.PokemonDao
import com.example.pokedex.database.PokemonDataBase
import com.example.pokedex.network.PokemonPagingSource
import com.example.pokedex.repository.DetalhesPokemonViewModel
import com.example.pokedex.repository.ListaPokemonViewModel
import com.example.pokedex.repository.PokemonRepository
import com.example.pokedex.repository.PokemonRepositoryImp
import com.example.pokedex.retrofit.PokemonRetrofit
import com.example.pokedex.retrofit.service.PokemonService
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModules = module {
    single<PokemonDataBase> {
        Room.databaseBuilder(
            get(),
            PokemonDataBase::class.java,
            "pokemon.db"
        )
            .build()
    }

    single<PokemonService> {
        PokemonRetrofit().getPokemonServices()
    }

    single<PokemonDao> {
        get<PokemonDataBase>().getPokemonDao()
    }
    single<PokemonPagingSource> {
        PokemonPagingSource(get(), get())
    }
    single<PokemonRepository> {
        PokemonRepositoryImp(get(), get())
    }

    viewModel<ListaPokemonViewModel> {
        ListaPokemonViewModel(get())
    }
    viewModel<DetalhesPokemonViewModel> {(id: Int)->
        DetalhesPokemonViewModel(get(), id)
    }

}
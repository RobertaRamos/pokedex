package com.example.pokedex.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.R
import com.example.pokedex.adapter.ListaPokemonAdapter
import com.example.pokedex.model.CHAVE_ID
import com.example.pokedex.model.CHAVE_POKEMON
import com.example.pokedex.model.Pokemon
import com.example.pokedex.repository.ListaPokemonViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListaPokemonActivity : AppCompatActivity() {

    private lateinit var adapter: ListaPokemonAdapter

    private val includeTelaErro by lazy {
        findViewById<View>(R.id.tela_erro)
    }
    private val progressBar by lazy {
        findViewById<ProgressBar>(R.id.lista_pokemon_progessbar)
    }

    private val viewModel by viewModel<ListaPokemonViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_pokemon)
        configuraAdapter()
        configuraRecyclerView()
        observa(viewModel)
        configuraBotaoTentarNovamente()

    }

    private fun configuraRecyclerView() {
        val recyclerView = findViewById<RecyclerView>(R.id.lista_pokemon_recyclerview)
        recyclerView.adapter = adapter
    }

    private fun configuraAdapter() {
        adapter = ListaPokemonAdapter()
        adapter.quandoItemClicado = { id: Int, pokemon: Pokemon ->
            vaiParaDetalhesPokemon(id, pokemon)
        }

    }
    private fun vaiParaDetalhesPokemon(id: Int, pokemon: Pokemon) {
        startActivity(
            Intent(this, DetalhesPokemonActivity::class.java)
                .putExtra(CHAVE_ID, id)
                .putExtra(CHAVE_POKEMON, pokemon)
        )
    }

    private fun observa(viewModel: ListaPokemonViewModel) {
        lifecycleScope.launchWhenCreated {
            viewModel.getPokemon().collectLatest {
                adapter.submitData(it)
            }
        }
        observaCarregamentoDaTela()
    }

    private fun observaCarregamentoDaTela() {
        lifecycleScope.launch {
            adapter.loadStateFlow.collectLatest { statusDeCarregamento ->
                progressBar.isVisible = statusDeCarregamento.refresh is LoadState.Loading
                includeTelaErro.isVisible = statusDeCarregamento.refresh is LoadState.Error
            }
        }
    }

    private fun configuraBotaoTentarNovamente() {
            val botaoTentarNovamente = findViewById<Button>(R.id.tela_erro_pokemon_botao)
            botaoTentarNovamente.setOnClickListener {
                observa(viewModel)
            }
        }

}
package com.example.pokedex.ui

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pokedex.R
import com.example.pokedex.adapter.ListaTipoPokemonAdapter
import com.example.pokedex.model.*
import com.example.pokedex.repository.DetalhesPokemonViewModel
import com.example.pokedex.retrofit.RetornoPokemonApiDetalhes
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class DetalhesPokemonActivity : AppCompatActivity() {

    private val progressbar by lazy {
        findViewById<View>(R.id.detalhes_pokemon_progressBar)
    }
    private lateinit var entidadePokemon: EntidadePokemon

    private val idPokemon by lazy {
        intent.getIntExtra(CHAVE_ID, POSICAO_INVALIDA)
    }

    private val viewModel: DetalhesPokemonViewModel by viewModel{ parametersOf(idPokemon)}

    private val botaoFavorito by lazy {
        findViewById<FloatingActionButton>(R.id.detalhes_pokemon_botao_favorito)
    }
    private var clicou = true

    private val recyclerView by lazy {
        findViewById<RecyclerView>(R.id.tipo_pokemon)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhes_pokemon)
        recebePokemon()


    }

    private fun recebePokemon() {
        if (intent.hasExtra(CHAVE_POKEMON)) {
            val pokemon = intent.getSerializableExtra(CHAVE_POKEMON) as Pokemon
            buscaPokemonPeloNome(pokemon.name)
            preencheCampo(pokemon.name)
            observaViewModelDetalhes()
            criaEntidadePokemon(pokemon)
            configuraBotaoFavorito()
        }
    }

    private fun criaEntidadePokemon(pokemon: Pokemon) {
        entidadePokemon = EntidadePokemon(name = pokemon.name, url = pokemon.url)
    }

    private fun buscaPokemonPeloNome(nome: String) {
        viewModel.buscaPokemon(nome).observe(this, Observer {
            if (it != null) {
                botaoFavorito.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_stat_name_estrela
                    )
                )
                clicou = false

            }
        })


    }


    private fun configuraBotaoFavorito() {
        botaoFavorito.setOnClickListener {
            clicou = if (clicou) {
                botaoFavorito.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_stat_name_estrela))
                viewModel.delegateSalvar(entidadePokemon)
                false

            } else {
                botaoFavorito.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.star_outline))
                viewModel.delegateRemove(entidadePokemon)
                true
            }
        }
    }

    private fun observaViewModelDetalhes() {
        viewModel.getPokemon().observe(this, {
            it.detalhes?.let { detalhes ->
                recyclerView.adapter = ListaTipoPokemonAdapter(detalhes.types)
                progressbar.visibility = View.GONE
                mostraAltura(detalhes.height)
                mostraPeso(detalhes.weight)
                mostraHabilidade(detalhes)
            }
        })
    }

    private fun mostraHabilidade(detalhes: RetornoPokemonApiDetalhes) {
        val campoHabilidade = findViewById<TextView>(R.id.detalhes_pokemon_valor_habilidades)
        val habilidade = detalhes.abilities[0].ability.name
        campoHabilidade.text = habilidade
    }

    private fun mostraPeso(peso: Double) {
        val campoPeso = findViewById<TextView>(R.id.detalhes_pokemon_valor_peso)
        val valorPeso = peso / 10
        campoPeso.text = "$valorPeso kg"
    }

    private fun mostraAltura(altura: Double) {
        val Campoaltura = findViewById<TextView>(R.id.detalhes_pokemon_valor_altura)
        val valorAltura = altura / 10
        Campoaltura.text = "$valorAltura m"


    }

    private fun preencheCampo(nome: String) {
        mostraImagem()
        mostraNome(nome)
    }

    private fun mostraNome(nome: String) {
        val campoNome = findViewById<CollapsingToolbarLayout>(R.id.detalhes_pokemon_collapsing)
        campoNome.title = nome
    }

    private fun mostraImagem() {
        val imagem = findViewById<ImageView>(R.id.detalhes_pokemon_imagem)
        Glide.with(imagem)
            .load(resources.getString(R.string.url_imagem, idPokemon))
            .into(imagem)
    }
}
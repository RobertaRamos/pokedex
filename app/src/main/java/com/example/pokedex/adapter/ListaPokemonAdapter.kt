package com.example.pokedex.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pokedex.R
import com.example.pokedex.model.Pokemon

class ListaPokemonAdapter() : PagingDataAdapter<Pokemon, ListaPokemonAdapter.ListaPokemonViewHolder>(DiffUtilCallBack()) {

    var quandoItemClicado: (id: Int,pokemon: Pokemon) -> Unit = { id: Int, pokemon: Pokemon -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListaPokemonViewHolder {
        val viewCriada = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
        return ListaPokemonViewHolder(viewCriada)
    }

    override fun onBindViewHolder(holder: ListaPokemonViewHolder, position: Int) {
        holder.vincula(getItem(position)!!)
    }

    inner class ListaPokemonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val campoImagem = itemView.findViewById<ImageView>(R.id.item_pokemon_imagem)
        val campoNome = itemView.findViewById<TextView>(R.id.item_pokemon_nome)
        val botaoDetalhes = itemView.findViewById<Button>(R.id.item_pokemon_botao)
        var id: Int = 0
        lateinit var pokemon: Pokemon


        fun configuraBotao() {
            botaoDetalhes.setOnClickListener(View.OnClickListener {
                if (::pokemon.isInitialized){
                    quandoItemClicado(id,pokemon)
                }

            })
        }

        fun vincula(pokemon: Pokemon) {
            this.pokemon = pokemon
            adicionaNome(pokemon.name)
            adicionaImagem(pokemon.url)
            configuraBotao()

        }

        private fun adicionaImagem(url: String) {
            val idPokemon = configuraRegex(url)
            idPokemon?.let {
                id = it
                mostraImagem(it)
            }

        }

        private fun mostraImagem(idPokemon: Int?) {
            Glide.with(campoImagem)
                .load(campoImagem.context.resources
                    .getString(R.string.url_imagem, idPokemon))
                .placeholder(R.mipmap.pokebola)
                .error(R.mipmap.pokebolavazia)
                .into(campoImagem)
        }

        private fun configuraRegex(url: String): Int? {
            val regex = """/\d+""".toRegex()
            return regex.find(url)?.value?.replace("/", "")?.toInt()

        }

        private fun adicionaNome(nome: String) {
            campoNome.text = nome
        }


    }

    class DiffUtilCallBack : DiffUtil.ItemCallback<Pokemon>() {
        override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
            return oldItem.name == newItem.name && oldItem.url == newItem.url
        }

    }
}
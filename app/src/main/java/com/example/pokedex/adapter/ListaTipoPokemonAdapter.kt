package com.example.pokedex.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.R
import com.example.pokedex.model.POKEMON_ICONE_TYPE
import com.example.pokedex.retrofit.TipoPokemon

class ListaTipoPokemonAdapter(private val tipos: List<TipoPokemon>) :
    RecyclerView.Adapter<ListaTipoPokemonAdapter.ListaTipoPokemonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListaTipoPokemonViewHolder {
        val viewCriada =
            LayoutInflater.from(parent.context).inflate(R.layout.tipo_pokemon, parent, false)

        return ListaTipoPokemonViewHolder(viewCriada)
    }

    override fun onBindViewHolder(holder: ListaTipoPokemonViewHolder, position: Int) {
        val tipoPokemon = tipos[position]

        holder.vincula(tipoPokemon)
    }

    override fun getItemCount(): Int {
        return tipos.size
    }

    inner class ListaTipoPokemonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val campoImagem = itemView.findViewById<ImageView>(R.id.tipo_pokemon_imagem)
        val context = campoImagem.context

        fun vincula(tipoPokemon: TipoPokemon) {
            val tipo = tipoPokemon.type.name
            mostraImagemTipo(tipo)
            mostraNomeTipo(tipo)
            adicionaCorNoTipo(tipo)
        }

        private fun adicionaCorNoTipo(tipo: String) {
            val tipoRetornado = POKEMON_ICONE_TYPE.getTipo(tipo)
            val corDoTipo = tipoRetornado.cor
            campoImagem.setColorFilter(ContextCompat.getColor(context, corDoTipo))
        }

        private fun mostraNomeTipo(tipo: String) {
            val campoNomeDoTipo = itemView.findViewById<TextView>(R.id.tipo_pokemon_nome)
            campoNomeDoTipo.text = tipo
        }

        private fun mostraImagemTipo(tipo: String) {
            val drawbleTipo = context.resources.getIdentifier("ic_" + tipo, "drawable", context.packageName)
            campoImagem.setImageResource(drawbleTipo)


        }
    }
}
package com.example.pokedex.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex.callback.RespostaCallback
import com.example.pokedex.model.EntidadePokemon
import com.example.pokedex.retrofit.RetornoPokemonApiDetalhes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetalhesPokemonViewModel(private val pokemonRepository: PokemonRepository,
                               private val id: Int): ViewModel() {

    fun getPokemon(): LiveData<Resource>{
        val mutableLiveData = MutableLiveData<Resource>()
        pokemonRepository.getPokemonAPI(object : RespostaCallback {
            override fun sucesso(detalhes: RetornoPokemonApiDetalhes) {
                mutableLiveData.postValue(Resource(detalhes = detalhes))
            }

            override fun falha(menssagemErro: String) {
                mutableLiveData.postValue(Resource(mensagemErro = menssagemErro))
            }

        },id)

        return mutableLiveData
    }

    fun delegateSalvar(entidadePokemon: EntidadePokemon){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                pokemonRepository.salvaPokemon(entidadePokemon)

            }
        }
    }
    fun delegateRemove(entidadePokemon: EntidadePokemon){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                pokemonRepository.removePokemon(entidadePokemon)
            }
        }

    }

    fun buscaPokemon(nome: String): LiveData<EntidadePokemon?>{
        return pokemonRepository.buscaPokemon(nome)

    }
}
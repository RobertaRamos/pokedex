package com.example.pokedex.repository

import androidx.lifecycle.LiveData
import com.example.pokedex.callback.RespostaCallback
import com.example.pokedex.dao.PokemonDao
import com.example.pokedex.model.EntidadePokemon
import com.example.pokedex.retrofit.RetornoPokemonApiDetalhes
import com.example.pokedex.retrofit.service.PokemonService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PokemonRepositoryImp(private val service: PokemonService, private val dao: PokemonDao): PokemonRepository {

    override fun getPokemonAPI(callback: RespostaCallback, id: Int) {
        val call = service.buscaDetalhesPokemon(id)
        call.enqueue(object : Callback<RetornoPokemonApiDetalhes> {
            override fun onResponse(call: Call<RetornoPokemonApiDetalhes>, response: Response<RetornoPokemonApiDetalhes>) {
                if(response.isSuccessful){
                    val resultado = response.body()
                    if(resultado != null){
                        callback.sucesso(resultado)
                    }else{
                        callback.falha("Resposta mal sucedida!")
                    }
                }
            }

            override fun onFailure(call: Call<RetornoPokemonApiDetalhes>, t: Throwable) {
                callback.falha("Falha de comunicação: " + t.message)
            }

        })
    }


    override fun salvaPokemon(pokemon: EntidadePokemon) {
        adicionaId(pokemon)
        dao.salva(pokemon)
    }

    override fun removePokemon(pokemon: EntidadePokemon) {
        adicionaId(pokemon)
        dao.remove(pokemon)
    }

    override fun buscaPokemon(nome: String): LiveData<EntidadePokemon?> {
        return dao.buscaPokemon(nome)
    }

    private fun adicionaId(pokemon: EntidadePokemon) {
        val todosPokemon = dao.buscaTodos()
        if(todosPokemon.isNotEmpty()){
                for(entidadePokemon in todosPokemon){
                    if(entidadePokemon.name == pokemon.name)
                        pokemon.id = entidadePokemon.id
                }
        }

    }
}
package com.example.pokedex.repository

import androidx.lifecycle.LiveData
import com.example.pokedex.callback.RespostaCallback
import com.example.pokedex.model.EntidadePokemon

interface PokemonRepository {
    fun getPokemonAPI(callback: RespostaCallback, id: Int)

    fun salvaPokemon(pokemon: EntidadePokemon)

    fun removePokemon(pokemon: EntidadePokemon)

    fun buscaPokemon(nome: String): LiveData<EntidadePokemon?>
}
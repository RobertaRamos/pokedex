package com.example.pokedex.repository

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.pokedex.dao.PokemonDao
import com.example.pokedex.database.PokemonDataBase
import com.example.pokedex.model.Pokemon
import com.example.pokedex.network.PokemonPagingSource
import com.example.pokedex.retrofit.PokemonRetrofit
import com.example.pokedex.retrofit.service.PokemonService
import kotlinx.coroutines.flow.Flow

class ListaPokemonViewModel(private val pagingSource: PokemonPagingSource) : ViewModel() {

    fun getPokemon(): Flow<PagingData<Pokemon>> {
        return Pager(config = PagingConfig(pageSize = 10, prefetchDistance = 50),
            pagingSourceFactory = {pagingSource}).flow.cachedIn(viewModelScope)
    }
}
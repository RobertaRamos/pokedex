package com.example.pokedex.repository

import com.example.pokedex.retrofit.RetornoPokemonApiDetalhes

class Resource(var detalhes: RetornoPokemonApiDetalhes? = null,
               var mensagemErro: String? = null)
